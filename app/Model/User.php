<?php
App::uses('AppModel', 'Model');
App::uses('BlowfishPasswordHasher', 'Controller/Component/Auth');

/**
 * User Model
 *
 */

class User extends AppModel
{


    /**
     * Use the containable behavior
     *
     * @var string
     */

     public $actsAs = array('Containable');

    /**
     * Set recursive default to -1
     *
     * @var string
     */

    public $recursive = -1;

    /**
    * Display field
    *
    * @var string
    */

    public $displayField = 'id';


    /**
    * Validationdomain
    *
    * Used for translating model error messages
    *
    * @var string
    */

    public $validationDomain = 'validation_errors';

    /**
     * Used in the controller to push through to views
     *
     * @var string $singName The singular name
     * @var string $plurName The plural name
     */

    public $singName;
    public $plurName;

    /**
     * Array containing the default values for schema generated inputs
     *
     * For automagic form element filling
     *
     * Controller:
     *
     * $basevalues = $this-><model>->basevalues;
     * $this->set(compact('basevalues')); //very important that it is called basevalues
     *
     * Model constructor:
     *
     * We put it in the model constructor so that we can apply i18n translations
     *
     * $this->baseValues = array(
     *     'language' => array(
     *         'nl' => __('nl'),
     *         'fr' => __('fr')
     *     )
     * );
     *
     * The Twitter bootstrap helper checks if the field name matches any given
     * top level array element and if so creates a select field and autopopulates
     * it with the correct values
     *
     * @var array $baseValues
     */

    public $baseValues = array();

    
    /**
     * Validation
     */
        
    public $validate = array(
        'username' => array(
            'required' => array(
                'rule' => array('notEmpty'),
                'message' => 'Gelieve een gebruikersnaam op te geven',
                'last' => true
            ),
            'unique' => array(
                'rule'    => 'isUnique',
                'message' => 'Deze gebruikersnaam bestaat reeds',
            ),
        ),
        'password' => array(
            'required' => array(
                'rule' => array('notEmpty'),
                'message' => 'Gelieve een wachtwoord in te vullen'
            )
        ),
        'role' => array(
            'valid' => array(
                'rule' => array('inList', array('admin', 'user')),
                'message' => 'Gelieve een geldige rol te kiezen',
                'allowEmpty' => false
            )
        )
    );
    
    /**
     * Override the cakephp constructor
     *
     * @param type $id
     * @param type $table
     * @param type $ds
     */

    public function __construct($id = false, $table = null, $ds = null)
    {
        parent::__construct($id, $table, $ds);

	$this->singName = __('gebruiker');
        $this->plurName = __('gebruikers');

        $this->baseValues = array();
    }
    
    public function beforeSave($options = array()) 
    {
        if (isset($this->data[$this->alias]['password'])) 
        {
            $passwordHasher = new BlowfishPasswordHasher();
            $this->data[$this->alias]['password'] = $passwordHasher->hash(
                $this->data[$this->alias]['password']
            );
        }
        
        return true;
    }
}