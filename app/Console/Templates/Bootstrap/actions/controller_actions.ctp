<?php
/**
 * Bake Template for Controller action generation.
 *
 * PHP 5
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright 2005-2012, Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright 2005-2012, Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       Cake.Console.Templates.default.actions
 * @since         CakePHP(tm) v 1.3
 * @license       MIT License (http://www.opensource.org/licenses/mit-license.php)
 */
?>

    /**
     * <?php echo $admin ?>index method
     *
     * @return void
     */
    
    public function <?php echo $admin ?>index() 
    {
        $this-><?php echo $currentModelName ?>->recursive = 0;
        $this->set('<?php echo $pluralName ?>', $this->paginate());
        $this->set('_serialize', array('<?php echo $pluralName ?>'));
    }

    /**
     * <?php echo $admin ?>view method
     *
     * @throws NotFoundException
     * @param string $id
     * @return void
     */
     
    public function <?php echo $admin ?>view($id = null) 
    {
        if (!$this-><?php echo $currentModelName; ?>->exists($id)) 
        {
            throw new BadRequestException(__('Ongeldige %s', $this->singname));
        }
        
        $options = array(
            'conditions' => array(
                '<?php echo $currentModelName; ?>.' . $this-><?php echo $currentModelName; ?>->primaryKey => $id
            ),
            'contain' => array(
<?php
    foreach (array('belongsTo', 'hasAndBelongsToMany', 'hasMany') as $assoc):
        foreach ($modelObj->{$assoc} as $associationName => $relation):
            if (!empty($associationName)):
                $otherModelName = $this->_modelName($associationName);
                $otherPluralName = $this->_pluralName($associationName);
                echo "\t\t'{$otherModelName}' => array(),\n";
            endif;
        endforeach;
    endforeach;
?>
            )
        );
        
        $this->set('<?php echo $singularName; ?>', $this-><?php echo $currentModelName; ?>->find('first', $options));
        $this->set('_serialize', array('<?php echo $singularName ?>'));
    }

<?php $compact = array(); ?>
    /**
     * <?php echo $admin ?>add method
     *
     * @return void
     */
     
    public function <?php echo $admin ?>add() 
    {
        if ($this->request->is('post')) 
        {
            $this-><?php echo $currentModelName; ?>->create();
            
            if ($this-><?php echo $currentModelName; ?>->save($this->request->data)) 
            {
<?php if ($wannaUseSession): ?>
                $this->Session->setFlash(__('De %s is bewaard', $this->singname), 'flash' . DS . 'success');
                $this->redirect(array('action' => 'index'));
<?php else: ?>
                $this->flash(__('De %s is bewaard', $this->singname), array('action' => 'index'));
<?php endif; ?>
            } 
            else 
            {
<?php if ($wannaUseSession): ?>
                $this->Session->setFlash(__('De %s kon niet worden bewaard, gelieve nogmaals te proberen', $this->singname), 'flash' . DS . 'error');
<?php endif; ?>
            }
        }
<?php
	foreach (array('belongsTo', 'hasAndBelongsToMany') as $assoc):
		foreach ($modelObj->{$assoc} as $associationName => $relation):
			if (!empty($associationName)):
				$otherModelName = $this->_modelName($associationName);
				$otherPluralName = $this->_pluralName($associationName);
				echo "\t\t\${$otherPluralName} = \$this->{$currentModelName}->{$otherModelName}->find('list');\n";
				$compact[] = "'{$otherPluralName}'";
			endif;
		endforeach;
	endforeach;
	if (!empty($compact)):
		echo "\t\t\$this->set(compact(".join(', ', $compact)."));\n";
		echo "\t\t\$this->set('_serialize', array(" . join(', ', $compact) . "));\n";
                
	endif;
?>
	}

<?php $compact = array(); ?>
    /**
     * <?php echo $admin ?>edit method
     *
     * @throws NotFoundException
     * @param string $id
     * @return void
     */
	
    public function <?php echo $admin; ?>edit($id = null) 
    {
        $this-><?php echo $currentModelName; ?>->id = $id;
        
        if (!$this-><?php echo $currentModelName; ?>->exists($id)) 
        {
            throw new BadRequestException(__('Ongeldige %s', $this->singname));
        }
        
        if ($this->request->is('post') || $this->request->is('put')) 
        {
            if ($this-><?php echo $currentModelName; ?>->save($this->request->data)) 
            {
<?php if ($wannaUseSession): ?>
                $this->Session->setFlash(__('De %s is bewaard', $this->singname), 'flash' . DS .'success');
                $this->redirect(array('action' => 'index'));
<?php else: ?>
                $this->flash(__('De %s is bewaard', $this->singname), array('action' => 'index'));
<?php endif; ?>
            } 
            else 
            {
<?php if ($wannaUseSession): ?>
                $this->Session->setFlash(__('De %s kon niet worden bewaard, gelieve nogmaals te proberen', $this->singname), 'flash' . DS . 'error');
<?php endif; ?>
            }
        } 
        else 
        {
            $options = array('conditions' => array('<?php echo $currentModelName; ?>.' . $this-><?php echo $currentModelName; ?>->primaryKey => $id));
            $this->request->data = $this-><?php echo $currentModelName; ?>->find('first', $options);
        }
<?php
		foreach (array('belongsTo', 'hasAndBelongsToMany') as $assoc):
			foreach ($modelObj->{$assoc} as $associationName => $relation):
				if (!empty($associationName)):
					$otherModelName = $this->_modelName($associationName);
					$otherPluralName = $this->_pluralName($associationName);
					echo "\t\t\${$otherPluralName} = \$this->{$currentModelName}->{$otherModelName}->find('list');\n";
					$compact[] = "'{$otherPluralName}'";
				endif;
			endforeach;
		endforeach;
		if (!empty($compact)):
			echo "\t\t\$this->set(compact(".join(', ', $compact)."));\n";
                        echo "\t\t\$this->set('_serialize', array(" . join(', ', $compact) . "));\n";
		endif;
	?>
	}

    /**
     * <?php echo $admin ?>delete method
     *
     * @throws NotFoundException
     * @throws MethodNotAllowedException
     * @param string $id
     * @return void
     */
    
    public function <?php echo $admin; ?>delete($id = null) 
    {
        if (!$this->request->is('post')) 
        {
            throw new MethodNotAllowedException();
        }
        
        $this-><?php echo $currentModelName; ?>->id = $id;
        if (!$this-><?php echo $currentModelName; ?>->exists()) 
        {
            throw new BadRequestException(__('Ongeldige %s', $this->singname));
        }
        
        if ($this-><?php echo $currentModelName; ?>->delete()) 
        {
<?php if ($wannaUseSession): ?>
            $this->Session->setFlash(__('De %s is verwijderd', $this->singname), 'flash' . DS . 'success');
            $this->redirect(array('action' => 'index'));
<?php else: ?>
            $this->flash(__('De %s is verwijderd', $this->singname), array('action' => 'index'));
<?php endif; ?>
        }
<?php if ($wannaUseSession): ?>
        
        $this->Session->setFlash(__('De %s kon niet worden verwijderd', $this->singname), 'flash' . DS . 'error');
<?php else: ?>
        $this->flash(__('De %s kon niet worden verwijderd', $this->singname), array('action' => 'index'));
<?php endif; ?>
        $this->redirect(array('action' => 'index'));
    }