<?php
/**
 *
 * PHP 5
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link http://cakephp.org CakePHP(tm) Project
 * @package Cake.Console.Templates.default.views
 * @since CakePHP(tm) v 1.2.0.5234
 * @license MIT License (http://www.opensource.org/licenses/mit-license.php)
 */
?>
<div id="page-container" class="row">
    <div id="sidebar" class="col-sm-3">
        <div class="actions">
            <ul class="list-group">
<?php
	if (strpos($action, 'add') === false) {
            echo "\t\t<li class=\"list-group-item\"><?php echo \$this->Form->postLink(__('Deze %s verwijderen', strtolower(\$singname)), array('action' => 'delete', \$this->Form->value('{$modelClass}.{$primaryKey}')), null, __('Ben je zeker dat je %s wil verwijderen?', \$this->Form->value('{$modelClass}.{$primaryKey}'))); ?></li>\n";
	}
    
        echo "\t\t<li class=\"list-group-item\"><?php echo \$this->Html->link(__('Toon alle %s', strtolower(\$plurname)), array('action' => 'index')); ?></li>\n";
	
	$done = array();
	foreach ($associations as $type => $data) {
            foreach ($data as $alias => $details) {
                if ($details['controller'] != $this->name && !in_array($details['controller'], $done)) {
                    echo "\t\t<li class=\"list-group-item\"><?php echo \$this->Html->link(__('Toon alle %s', strtolower(\$" . strtolower(Inflector::singularize($details['controller'])) . "_plurname)), array('controller' => '{$details['controller']}', 'action' => 'index')); ?> </li>\n";
                    echo "\t\t<li class=\"list-group-item\"><?php echo \$this->Html->link(__('Een nieuwe %s toevoegen', strtolower(\$" . strtolower(Inflector::singularize($details['controller'])) . "_singname)), array('controller' => '{$details['controller']}', 'action' => 'add')); ?> </li>\n";
                    $done[] = $details['controller'];
                }
            }
	}
?>
            </ul>	
        </div>
    </div>
	
    <div id="page-content" class="col-sm-9">
        <?php
            switch(Inflector::humanize($action))
            {
                case 'Add':
                    $actie = 'toevoegen';
                    break;
                case 'Edit':
                    $actie = 'aanpassen';
                    break;
            }
        ?>
        
        <h2><?php printf("<?php echo __('Een %s %s', \$singname); ?>", '%s', $actie); ?></h2>

       	
        <?php echo "<?php echo \$this->BootstrapForm->create('{$modelClass}', array('role' => 'form', 'novalidate' => true)); ?>\n"; ?>

<?php
	foreach ($fields as $field) {
		if (strpos($action, 'add') !== false && $field == $primaryKey) {
			continue;
		} elseif (!in_array($field, array('created', 'modified', 'updated'))) {
			echo "\t    <?php echo \$this->BootstrapForm->input('{$field}', array('class' => 'form-control', 'label' => '{$field}')); ?>\n";

		}
	}
	if (!empty($associations['hasAndBelongsToMany'])) {
		foreach ($associations['hasAndBelongsToMany'] as $assocName => $assocData) {
			echo "\t    <?php echo \$this->BootstrapForm->input('{$assocName}', array('class' => 'form-control', 'label' => '{$assocName}'));?>\n";
		}
	}
	echo "\n";
	echo "\t<?php echo \$this->BootstrapForm->submit(__('Opslaan'), array('class' => 'btn btn-large btn-primary')); ?>\n";
?>

<?php
    echo "\t<?php echo \$this->BootstrapForm->end(); ?>";
?>
		
    </div>
</div>