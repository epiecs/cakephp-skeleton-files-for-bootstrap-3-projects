<?php
/**
 *
 * PHP 5
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       Cake.Console.Templates.default.views
 * @since         CakePHP(tm) v 1.2.0.5234
 * @license       MIT License (http://www.opensource.org/licenses/mit-license.php)
 */
?>
<div id="page-container" class="row">
    <div id="sidebar" class="col-sm-3">		
        <div class="actions">
            <ul class="list-group">
                <li class="list-group-item"><?php echo "<?php echo \$this->Html->link(__('Een nieuwe %s toevoegen', strtolower(\$singname)), array('action' => 'add'), array('class' => '')); ?>"; ?></li>
<?php
	$done = array();
	foreach ($associations as $type => $data) {
            foreach ($data as $alias => $details) {
                if ($details['controller'] != $this->name && !in_array($details['controller'], $done)) {
                    echo "\t\t<li class=\"list-group-item\"><?php echo \$this->Html->link(__('Toon alle %s', strtolower(\$" . strtolower(Inflector::singularize($details['controller'])) . "_plurname)), array('controller' => '{$details['controller']}', 'action' => 'index'), array('class' => '')); ?></li> \n";
                    echo "\t\t<li class=\"list-group-item\"><?php echo \$this->Html->link(__('Een nieuwe %s toevoegen', strtolower(\$" . strtolower(Inflector::singularize($details['controller'])) . "_singname)), array('controller' => '{$details['controller']}', 'action' => 'add'), array('class' => '')); ?></li> \n";
                    $done[] = $details['controller'];
                }
            }
	}
?>
            </ul>	
        </div>		
    </div>
	
    <div id="page-content" class="col-sm-9">

        <div class="<?php echo $pluralVar; ?> index">
		
            <h2><?php echo "<?php echo ucfirst(\$plurname); ?>"; ?></h2>
			
            <div class="table-responsive">
                <table cellpadding="0" cellspacing="0" class="table table-striped table-bordered">
                    <thead>
                        <tr>
<?php foreach ($fields as $field): ?>
                            <th><?php echo "<?php echo \$this->Paginator->sort('{$field}', '{$field}'); ?>"; ?></th>
<?php endforeach; ?>
				
                            <th class="actions"><?php echo "<?php echo __('Acties'); ?>"; ?></th>
                        </tr>
                    </thead>
                        
                    <tbody>
<?php 
        echo "\t\t\t<?php foreach (\${$pluralVar} as \${$singularVar}): ?>\n";
	echo "\t\t\t    <tr>\n";
	foreach ($fields as $field) {
		$isKey = false;
		if (!empty($associations['belongsTo'])) {
                    foreach ($associations['belongsTo'] as $alias => $details) {
                        if ($field === $details['foreignKey']) {
                            $isKey = true;
                            echo "\t\t\t\t<td>\n\t\t\t\t    <?php echo \$this->Html->link(\${$singularVar}['{$alias}'][\$" . strtolower($alias) . "_displayfield], array('controller' => '{$details['controller']}', 'action' => 'view', \${$singularVar}['{$alias}']['{$details['primaryKey']}'])); ?>\n\t\t\t\t</td>\n";
                            break;
                        }
                    }
		}
		if ($isKey !== true) {
			echo "\t\t\t\t<td><?php echo h(\${$singularVar}['{$modelClass}']['{$field}']); ?>&nbsp;</td>\n";
		}
	}

	echo "\t\t\t\t<td class=\"actions\">\n";
	echo "\t\t\t\t    <?php echo \$this->Html->link(__('Bekijken'), array('action' => 'view', \${$singularVar}['{$modelClass}']['{$primaryKey}']), array('class' => 'btn btn-default btn-xs')); ?>\n";
	echo "\t\t\t\t    <?php echo \$this->Html->link(__('Aanpassen'), array('action' => 'edit', \${$singularVar}['{$modelClass}']['{$primaryKey}']), array('class' => 'btn btn-info btn-xs')); ?>\n";
	echo "\t\t\t\t    <?php echo \$this->Form->postLink(__('Verwijderen'), array('action' => 'delete', \${$singularVar}['{$modelClass}']['{$primaryKey}']), array('class' => 'btn btn-danger btn-xs'), __('Ben je zeker dat je %s wil verwijderen?', \${$singularVar}['{$modelClass}']['{$primaryKey}'])); ?>\n";
	echo "\t\t\t\t</td>\n";
	echo "\t\t\t    </tr>\n";

	echo "\t\t\t<?php endforeach; ?>\n";
?>
                    </tbody>
                </table>

            </div>
			
            <p>
                <small>
                    <?php echo "<?php
                        echo \$this->Paginator->counter(array(
                            'format' => __('Pagina {:page} van de {:pages}, momenteel worden er {:current} record(s) van de {:count} in totaal getoond, startend op {:start} en eindigend op {:end}')
                        ));
                    ?>\n"; ?>
                </small>
            </p>

            <ul class="pagination">
                <?php
                echo "<?php\n";
                    echo "\t\t    echo \$this->Paginator->prev('< ' . __('Vorige'), array('tag' => 'li'), null, array('class' => 'disabled', 'tag' => 'li', 'disabledTag' => 'a'));\n";
                    echo "\t\t    echo \$this->Paginator->numbers(array('separator' => '', 'currentTag' => 'a', 'tag' => 'li', 'currentClass' => 'disabled'));\n";
                    echo "\t\t    echo \$this->Paginator->next(__('Volgende') . ' >', array('tag' => 'li'), null, array('class' => 'disabled', 'tag' => 'li', 'disabledTag' => 'a'));\n";
                echo "\t\t?>\n";
                ?>
            </ul>
			
        </div>
    </div>
</div>