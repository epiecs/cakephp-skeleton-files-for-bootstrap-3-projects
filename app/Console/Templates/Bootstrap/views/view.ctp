<?php
/**
 *
 * PHP 5
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright 2005-2012, Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright 2005-2012, Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       Cake.Console.Templates.default.views
 * @since         CakePHP(tm) v 1.2.0.5234
 * @license       MIT License (http://www.opensource.org/licenses/mit-license.php)
 */
?>
<div id="page-container" class="row">
    <div id="sidebar" class="col-sm-3">
		
        <div class="actions">
			
            <ul class="list-group">			
<?php
                echo "\t\t<li class=\"list-group-item\"><?php echo \$this->Html->link(__('Deze %s aanpassen', strtolower(\$singname)), array('action' => 'edit', \${$singularVar}['{$modelClass}']['{$primaryKey}']), array('class' => '')); ?> </li>\n";
                echo "\t\t<li class=\"list-group-item\"><?php echo \$this->Form->postLink(__('Deze %s verwijderen', strtolower(\$singname)), array('action' => 'delete', \${$singularVar}['{$modelClass}']['{$primaryKey}']), array('class' => ''), __('Ben je zeker dat je %s wil verwijderen?', \${$singularVar}['{$modelClass}']['{$primaryKey}'])); ?> </li>\n";
                echo "\t\t<li class=\"list-group-item\"><?php echo \$this->Html->link(__('Toon alle %s', strtolower(\$plurname)), array('action' => 'index'), array('class' => '')); ?> </li>\n";
                echo "\t\t<li class=\"list-group-item\"><?php echo \$this->Html->link(__('Een nieuwe %s toevoegen', strtolower(\$singname)), array('action' => 'add'), array('class' => '')); ?> </li>";
                $done = array();
                foreach ($associations as $type => $data) {
                    foreach ($data as $alias => $details) {
                        if ($details['controller'] != $this->name && !in_array($details['controller'], $done)) {
                            echo "\t\t<li class=\"list-group-item\"><?php echo \$this->Html->link(__('Toon alle %s', strtolower(\$" . strtolower(Inflector::singularize($details['controller'])) . "_plurname)), array('controller' => '{$details['controller']}', 'action' => 'index'), array('class' => '')); ?> </li>\n";
                            echo "\t\t<li class=\"list-group-item\"><?php echo \$this->Html->link(__('Een nieuwe %s toevoegen', strtolower(\$" . strtolower(Inflector::singularize($details['controller'])) . "_singname)), array('controller' => '{$details['controller']}', 'action' => 'add'), array('class' => '')); ?> </li>\n";
                            $done[] = $details['controller'];
                        }
                    }
                }
?>				
            </ul>	
        </div>
    </div>
	
    <div id="page-content" class="col-sm-9">	
        <div class="<?php echo $pluralVar; ?> view">
            
            <h2><?php echo "<?php  echo __('%s fiche voor %s', ucfirst(\$plurname), \${$singularVar}['{$modelClass}'][\$displayfield]); ?>"; ?></h2>
			
            <div class="table-responsive">
                <table class="table table-striped table-bordered">
                    <tbody>
<?php
                    foreach ($fields as $field) {
        $isKey = false;
        if (!empty($associations['belongsTo'])) {
                foreach ($associations['belongsTo'] as $alias => $details) {
                        if ($field === $details['foreignKey']) {
                                $isKey = true;
                                echo "\t\t\t<tr>\n";
                                echo "\t\t\t    <td><strong><?php echo ucfirst(\$" . strtolower($alias) . "_singname); ?></strong></td>\n";
                                echo "\t\t\t    <td><?php echo \$this->Html->link(\${$singularVar}['{$alias}'][\$" . strtolower($alias) . "_displayfield], array('controller' => '{$details['controller']}', 'action' => 'view', \${$singularVar}['{$alias}']['{$details['primaryKey']}']), array('class' => '')); ?>&nbsp;</td>\n";
                                echo "\t\t\t</tr>";
                                break;
                        }
                }
        }
        if ($isKey !== true) {
                echo "\t\t\t<tr>\n";
                echo "\t\t\t    <td><strong><?php echo __('" . Inflector::humanize($field) . "'); ?></strong></td>\n";
                echo "\t\t\t    <td><?php echo h(\${$singularVar}['{$modelClass}']['{$field}']); ?>&nbsp;</td>\n";
                echo "\t\t\t</tr>\n";
        }
}
?>
                    </tbody>
                </table>
            </div>	
        </div>

            <?php
            if (!empty($associations['hasOne'])) :
                    foreach ($associations['hasOne'] as $alias => $details): ?>
                            <div class="related">
                                    <h3><?php echo "<?php echo __('Related " . Inflector::humanize($details['controller']) . "'); ?>"; ?></h3>
                                    <?php echo "<?php if (!empty(\${$singularVar}['{$alias}'])): ?>\n"; ?>
                                            <table class="table table-striped table-bordered">
                                                    <?php
                                                    foreach ($details['fields'] as $field) {
                                                            echo "<tr>";
                                                            echo "\t\t<td><strong><?php echo __('" . Inflector::humanize($field) . "'); ?></strong></td>\n";
                                                            echo "\t\t<td><strong><?php echo \${$singularVar}['{$alias}']['{$field}']; ?>\n&nbsp;</strong></td>\n";
                                                            echo "</tr>";
                                                    }
                                                    ?>
                                            </table><!-- /.table table-striped table-bordered -->
                                    <?php echo "<?php endif; ?>\n"; ?>
                                    <div class="actions">
                                            <?php echo "<li><?php echo \$this->Html->link(__('<i class=\"icon-pencil icon-white\"></i> Edit " . Inflector::humanize(Inflector::underscore($alias)) . "'), array('controller' => '{$details['controller']}', 'action' => 'edit', \${$singularVar}['{$alias}']['{$details['primaryKey']}']), array('class' => 'btn btn-primary', 'escape' => false)); ?>\n"; ?>
                                    </div><!-- /.actions -->
                            </div><!-- /.related -->
<?php
                    endforeach;
            endif;

            if (empty($associations['hasMany'])) {
                    $associations['hasMany'] = array();
            }
            if (empty($associations['hasAndBelongsToMany'])) {
                    $associations['hasAndBelongsToMany'] = array();
            }
            $relations = array_merge($associations['hasMany'], $associations['hasAndBelongsToMany']);
            $i = 0;
            foreach ($relations as $alias => $details):
                    $otherSingularVar = Inflector::variable($alias);
                    $otherPluralHumanName = Inflector::humanize($details['controller']);
?>
                            
        <div class="related">
            <h3><?php echo "<?php echo __('Aanverwante %s', strtolower(\$" . strtolower(Inflector::singularize($otherPluralHumanName)) . "_plurname)); ?>"; ?></h3>
            <?php echo "<?php if (!empty(\${$singularVar}['{$alias}'])): ?>\n"; ?>

            <div class="table-responsive">
                <table class="table table-striped table-bordered">
                    <thead>
                        <tr>
<?php
                                foreach ($details['fields'] as $field) {
                                    echo "\t\t\t    <th><?php echo __('" . Inflector::humanize($field) . "'); ?></th>\n";
                                }
?>
                            <th class="actions"><?php echo "<?php echo __('Acties'); ?>"; ?></th>
                        </tr>
                    </thead>
                    <tbody>
<?php
                    echo "\t\t    <?php
                        \$i = 0;
                        foreach (\${$singularVar}['{$alias}'] as \${$otherSingularVar}): ?>\n";
                    echo "\t\t\t<tr>\n";
                    foreach ($details['fields'] as $field) {
                            echo "\t\t\t    <td><?php echo \${$otherSingularVar}['{$field}']; ?></td>\n";
                    }

                        echo "\t\t\t    <td class=\"actions\">\n";
                        echo "\t\t\t\t<?php echo \$this->Html->link(__('Bekijken'), array('controller' => '{$details['controller']}', 'action' => 'view', \${$otherSingularVar}['{$details['primaryKey']}']), array('class' => 'btn btn-default btn-xs')); ?>\n";
                        echo "\t\t\t\t<?php echo \$this->Html->link(__('Aanpassen'), array('controller' => '{$details['controller']}', 'action' => 'edit', \${$otherSingularVar}['{$details['primaryKey']}']), array('class' => 'btn btn-info btn-xs')); ?>\n";
                        echo "\t\t\t\t<?php echo \$this->Form->postLink(__('Verwijderen'), array('controller' => '{$details['controller']}', 'action' => 'delete', \${$otherSingularVar}['{$details['primaryKey']}']), array('class' => 'btn btn-danger btn-xs'), __('Ben je zeker dat je %s wil verwijderen?', \${$otherSingularVar}['{$details['primaryKey']}'])); ?>\n";
                        echo "\t\t\t    </td>\n";
                    echo "\t\t\t</tr>\n";

                    echo "\t\t\t<?php endforeach; ?>\n";
?>
                    </tbody>
                </table>
            </div>

            <?php echo "<?php endif; ?>\n\n"; ?>

            <div class="actions">
                <?php echo "<?php echo \$this->Html->link('<i class=\"icon-plus icon-white\"></i> '.__('Een nieuwe %s toevoegen', strtolower(\$" . strtolower(Inflector::singularize($alias)) . "_singname)), array('controller' => '{$details['controller']}', 'action' => 'add'), array('class' => 'btn btn-primary', 'escape' => false)); ?>"; ?>
            </div>

        </div>
        <?php endforeach; ?>

    </div>
</div>
