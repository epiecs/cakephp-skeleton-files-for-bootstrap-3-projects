<?php
App::uses('AppController', 'Controller');
/**
 * Users Controller
 *
 * @property User $User
 * @property PaginatorComponent $Paginator
 */
class UsersController extends AppController 
{

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator');

    var $name = "Users";

    function startupProcess()
    {
	$this->singname = __("gebruiker");
	$this->plurname = __("gebruikers");	
		
	$this->set("singname", $this->User->singName);
	$this->set("plurname", $this->User->plurName);
	
	$this->set("displayfield", $this->User->displayField);
		
	//prep array for generating breadcrumbs

	$this->breadcrumbs = array(
	    ucfirst($this->plurname) => "index"
	);
	
	parent::startupProcess();
    }

    function beforefilter()
    {		
	parent::beforeFilter();
	
	$this->Auth->allow('add', 'login', 'logout');
	$this->set("title_for_layout", $this->plurname);
    
    }

/**
     * index method
     *
     * @return void
     */
    
    public function index() 
    {
        $this->User->recursive = 0;
        $this->set('users', $this->paginate());
    }

    /**
     * view method
     *
     * @throws NotFoundException
     * @param string $id
     * @return void
     */
     
    public function view($id = null) 
    {
        if (!$this->User->exists($id)) 
        {
            throw new NotFoundException(__('Ongeldige %s', $this->singname));
        }
        
        $options = array('conditions' => array('User.' . $this->User->primaryKey => $id));
        $this->set('user', $this->User->find('first', $options));
    }

    /**
     * add method
     *
     * @return void
     */
     
    public function add() 
    {
        if ($this->request->is('post')) 
        {
            $this->User->create();
            
            if ($this->User->save($this->request->data)) 
            {
                $this->Session->setFlash(__('De %s is bewaard', $this->singname), 'flash' . DS . 'success');
                $this->redirect(array('action' => 'index'));
            } 
            else 
            {
                $this->Session->setFlash(__('De %s kon niet worden bewaard, gelieve nogmaals te proberen.', $this->singname), 'flash' . DS . 'error');
            }
        }
	}

    /**
     * edit method
     *
     * @throws NotFoundException
     * @param string $id
     * @return void
     */
	
    public function edit($id = null) 
    {
        $this->User->id = $id;
        
        if (!$this->User->exists($id)) 
        {
            throw new NotFoundException(__('Invalid user'));
        }
        
        if ($this->request->is('post') || $this->request->is('put')) 
        {
            if ($this->User->save($this->request->data)) 
            {
                $this->Session->setFlash(__('De %s is bewaard', $this->singname), 'flash' . DS . 'success');
                $this->redirect(array('action' => 'index'));
            } 
            else 
            {
                $this->Session->setFlash(__('De %s kon niet worden bewaard, gelieve nogmaals te proberen.', $this->singname), 'flash' . DS . 'error');
            }
        } 
        else 
        {
            $options = array('conditions' => array('User.' . $this->User->primaryKey => $id));
            $this->request->data = $this->User->find('first', $options);
        }
	}

    /**
     * delete method
     *
     * @throws NotFoundException
     * @throws MethodNotAllowedException
     * @param string $id
     * @return void
     */
    
    public function delete($id = null) 
    {
        if (!$this->request->is('post')) 
        {
            throw new MethodNotAllowedException();
        }
        
        $this->User->id = $id;
        if (!$this->User->exists()) 
        {
            throw new NotFoundException(__('Ongeldige %s', $this->singname));
        }
        
        if ($this->User->delete()) 
        {
            $this->Session->setFlash(__('De %s werd verwijderd', $this->singname), 'flash' . DS . 'success');
            $this->redirect(array('action' => 'index'));
        }
        
        $this->Session->setFlash(__('De %s kon niet worden verwijderd', $this->singname), 'flash' . DS . 'error');
        $this->redirect(array('action' => 'index'));
    }
    
    public function login() 
    {
        $this->layout = 'login';
        
        if ($this->request->is('post')) {
            if ($this->Auth->login()) 
            {
                $this->Session->setFlash(__('U bent ingelogd'), 'flash' . DS . 'success');
                return $this->redirect($this->Auth->redirect());
            }
            $this->Session->setFlash(__('Ongeldige gebruikersnaam en/of wachtwoord'), 'flash' . DS . 'error');
        }
    }

    public function logout() 
    {
        $this->Session->setFlash(__('U bent uitgelogd'), 'flash' . DS . 'success');
        return $this->redirect($this->Auth->logout());
    }
}
