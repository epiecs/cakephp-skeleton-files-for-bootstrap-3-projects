<?php
/**
 * Application level Controller
 *
 * This file is application-wide controller file. You can put all
 * application-wide controller-related methods here.
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.Controller
 * @since         CakePHP(tm) v 0.2.9
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */

App::uses('Controller', 'Controller');
App::uses('Xml', 'Utility');

/**
 * Application Controller
 *
 * Add your application-wide methods in the class below, your controllers
 * will inherit them.
 *
 * @package		app.Controller
 * @link		http://book.cakephp.org/2.0/en/controllers.html#the-app-controller
 */
class AppController extends Controller 
{
    
    public $components = array(
	'RequestHandler',
        'Cookie',
        'Auth',
	'Session'
    );

    public $helpers = array
    (
	'Bootstrap' => array(
	    'className' => 'Bs'
	),
	'BootstrapForm' => array(
	    'className' => 'BsForm'
	),
	'Session',
        'Text'
    );

    public function beforeFilter()
    {
        /**
         * Op basis van de extensie de data omzetten Indien er een van de nodige extensies set is 
         * geven we een extensie terug indien de user niet ingelogd is
         * 
         * Indien er geen extensie geset is van de mogelijk apicalls wordt er gewoon standaard form
         * authentication gebruikt
         * 
         * Dit heeft ook geen impact op eventuele xml views wanneer je gewoon form authentication 
         * gebruikt omdat dan al de sessie gestart is en auth geskipt wordt zo lang de sessie geldt
         */
                
        $this->request->params['ext'] = isset($this->request->params['ext']) ? $this->request->params['ext'] : '';
        
        switch($this->request->params['ext'])
        {
            case 'json':   
            case 'xml':
                    $this->Auth->authenticate = array(
                        'Basic' => array(
                            'passwordHasher' => 'Simple'
                    ));
                
                    /*
                     * Indien we een api call hebben gebruiken de standaard unauthorized redirect. Indien
                     * de user niet ingelogd is wordt er een 401 gereturnd
                     */
                
                    $this->Auth->unauthorizedRedirect = false;
                
                break;
            default:
                    $this->Auth->authenticate = array(
                        'Form' => array(
                            'passwordHasher' => 'Simple'
                        )
                    );
                    
                    $this->Auth->flash = array(
                        'element' => 'flash' . DS . 'error',
                        'key' => 'auth',
                        'params' => array()
                    );

                    $this->Auth->loginRedirect = array(
                        'controller' => 'users',
                        'action' => 'index'
                    );

                    $this->Auth->logoutRedirect = array(
                        'controller' => 'pages',
                        'action' => 'display',
                        'home'
                    );
                    
                break;
        }
        
        /**
         * Indien er api data is deze gelijk omzetten naar een correcte request->data array
         */
        
        $data = $this->request->input() != '' ? $this->request->input() : '';
       
        if($data != '' && isset($headers['Content-Type']))
        {  
            /**
             * Op basis van de header de data omzetten
             */
            
            switch($this->request->params['ext'])
            {
                case 'json':   
                        $datarq = json_decode($data, true);
                        $this->request->data = $datarq;
                    break;
                case 'xml':
                        $datarq = Xml::toArray(Xml::build($data, array('return' => 'simplexml')));
                        $this->request->data = $datarq;
                    break;
                default:
                    break;
            }
        }
    }
}
