<?php echo $this->Html->docType('html5'); ?> 
<html>
	<head>
		<?php echo $this->Html->charset(); ?>
		<title>
			<?php echo $title_for_layout; ?>
		</title>
		<?php
			echo $this->Html->meta('icon');
			
			echo $this->fetch('meta');

			echo $this->Html->css('bootstrap');
			echo $this->Html->css('datepicker');
			echo $this->Html->css('main');

			echo $this->fetch('css');
			
			echo $this->Html->script('libs/jquery-1.10.2.min');
			echo $this->Html->script('libs/bootstrap.min');
			
			echo $this->fetch('script');
		?>
	</head>

	<body>
		<div id="main-container">
		
                    <div class="navbar navbar-default navbar-inverse navbar-fixed-top" role="navigation">
                        <div class="container">
                            <?php echo $this->element('navigation' . DS . 'main'); ?>
                        </div>
                    </div>

                    <div id="content" class="container">
                        <?php echo $this->Session->flash(); ?>
                        <?php echo $this->fetch('content'); ?>
                    </div>

                    <div id="footer" class="container">
                        &copy; <?php echo Configure::read('Company.name'); ?>
                    </div>
		</div>
		
		<div class="container">
                    <?php echo $this->element('sql_dump'); ?>
		</div>
		
	</body>

</html>