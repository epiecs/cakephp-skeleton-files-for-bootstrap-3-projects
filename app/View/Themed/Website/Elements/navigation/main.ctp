<div class="navbar-header">
    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target=".navbar-collapse">
        <span class="sr-only">Navigatie openen</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
    </button>
    <a class="navbar-brand" href="#"><?php echo Configure::read('Project.name'); ?></a>
</div>
<div class="navbar-collapse collapse">
    <ul class="nav navbar-nav">
        <li class="active">
            <a href="#">Home</a>
        </li>
        
        <li class="dropdown">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">Dropdown <span class="caret"></span></a>
            <ul class="dropdown-menu" role="menu">
                <li>
                    <a href="#">Actie 1</a>
                </li>
                <li>
                    <a href="#">Actie 2</a>
                </li>
                <li class="divider"></li>
                <li class="dropdown-header">Nav header</li>
                <li>
                    <a href="#">Actie 3</a>
                </li>
                <li>
                    <a href="#">Actie 4</a>
                </li>
            </ul>
        </li>
    </ul>
    <ul class="nav navbar-nav navbar-right">
        <li>
            <?php echo $this->Html->link('Admin', Configure::read('Admin.mainpage')); ?>
        </li>
    </ul>
</div><!--/.nav-collapse -->