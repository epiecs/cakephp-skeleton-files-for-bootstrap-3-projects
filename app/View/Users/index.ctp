<div id="page-container" class="row">
    <div id="sidebar" class="col-sm-3">		
        <div class="actions">
            <ul class="list-group">
                <li class="list-group-item"><?php echo $this->Html->link(__('Een nieuwe %s toevoegen', $singname), array('action' => 'add'), array('class' => '')); ?></li>
            </ul>	
        </div>		
    </div>
	
    <div id="page-content" class="col-sm-9">

        <div class="users index">
		
            <h2><?php echo ucfirst($plurname); ?></h2>
			
            <div class="table-responsive">
                <table cellpadding="0" cellspacing="0" class="table table-striped table-bordered">
                    <thead>
                        <tr>
                            <th><?php echo $this->Paginator->sort('username', 'Gebruikersnaam'); ?></th>
                            <th><?php echo $this->Paginator->sort('role', 'Rol'); ?></th>
                            <th><?php echo $this->Paginator->sort('created', 'Aangemaakt'); ?></th>
                            <th><?php echo $this->Paginator->sort('modified', 'Aangepast'); ?></th>
				
                            <th class="actions"><?php echo __('Acties'); ?></th>
                        </tr>
                    </thead>
                        
                    <tbody>
			<?php foreach ($users as $user): ?>
			    <tr>
				<td><?php echo h($user['User']['username']); ?>&nbsp;</td>
				<td><?php echo h($user['User']['role']); ?>&nbsp;</td>
				<td><?php echo h($user['User']['created']); ?>&nbsp;</td>
				<td><?php echo h($user['User']['modified']); ?>&nbsp;</td>
				<td class="actions">
				    <?php echo $this->Html->link(__('Bekijken'), array('action' => 'view', $user['User']['id']), array('class' => 'btn btn-default btn-xs')); ?>
				    <?php echo $this->Html->link(__('Aanpassen'), array('action' => 'edit', $user['User']['id']), array('class' => 'btn btn-info btn-xs')); ?>
				    <?php echo $this->Form->postLink(__('Verwijderen'), array('action' => 'delete', $user['User']['id']), array('class' => 'btn btn-danger btn-xs'), __('Are you sure you want to delete # %s?', $user['User']['id'])); ?>
				</td>
			    </tr>
			<?php endforeach; ?>
                    </tbody>
                </table>

            </div>
			
            <p>
                <small>
                    <?php
                        echo $this->Paginator->counter(array(
                            'format' => __('Pagina {:page} van de {:pages}, momenteel worden er {:current} record(s) van de {:count} in totaal getoond, startend op {:start} en eindigend op {:end}')
                        ));
                    ?>
                </small>
            </p>

            <ul class="pagination">
                <?php
		    echo $this->Paginator->prev('< ' . __('Vorige'), array('tag' => 'li'), null, array('class' => 'disabled', 'tag' => 'li', 'disabledTag' => 'a'));
		    echo $this->Paginator->numbers(array('separator' => '', 'currentTag' => 'a', 'tag' => 'li', 'currentClass' => 'disabled'));
		    echo $this->Paginator->next(__('Volgende') . ' >', array('tag' => 'li'), null, array('class' => 'disabled', 'tag' => 'li', 'disabledTag' => 'a'));
		?>
            </ul>
			
        </div>
    </div>
</div>