<?php echo $this->Form->create('User', array('class' => 'form-signin', 'novalidate' => true)); ?>
    <h2 class="form-signin-heading">Gelieve in te loggen</h2>
    
    <?php echo $this->Form->input('username', array(
        'class' => 'form-control',
        'label' => false,
        'placeholder' => 'Gebruikersnaam',
        'autofocus' => true
    )); ?>
    <?php echo $this->Form->input('password', array(
        'class' => 'form-control',
        'label' => false,
        'placeholder' => 'Wachtwoord'
    )); ?>
    <?php echo $this->Form->submit(__('Inloggen'), array('class' => 'btn btn-lg btn-primary btn-block')); ?>
    
<?php echo $this->BootstrapForm->end(); ?>