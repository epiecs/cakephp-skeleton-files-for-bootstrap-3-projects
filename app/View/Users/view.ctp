<div id="page-container" class="row">
    <div id="sidebar" class="col-sm-3">
		
        <div class="actions">
			
            <ul class="list-group">			
		<li class="list-group-item"><?php echo $this->Html->link(__('Deze %s aanpassen', $singname), array('action' => 'edit', $user['User']['id']), array('class' => '')); ?> </li>
		<li class="list-group-item"><?php echo $this->Form->postLink(__('Deze %s verwijderen', $singname), array('action' => 'delete', $user['User']['id']), array('class' => ''), __('Ben je zeker dat je %s wil verwijderen?', $user['User']['id'])); ?> </li>
		<li class="list-group-item"><?php echo $this->Html->link(__('Toon alle %s', $plurname), array('action' => 'index'), array('class' => '')); ?> </li>
		<li class="list-group-item"><?php echo $this->Html->link(__('Een nieuwe %s toevoegen', $singname), array('action' => 'add'), array('class' => '')); ?> </li>				
            </ul>	
        </div>
    </div>
	
    <div id="page-content" class="col-sm-9">	
        <div class="users view">
            
            <h2><?php  echo __('%s fiche voor %s', ucfirst($plurname), $user['User']['id']); ?></h2>
			
            <div class="table-responsive">
                <table class="table table-striped table-bordered">
                    <tbody>
			<tr>
			    <td><strong><?php echo __('Id'); ?></strong></td>
			    <td><?php echo h($user['User']['id']); ?>&nbsp;</td>
			</tr>
			<tr>
			    <td><strong><?php echo __('Username'); ?></strong></td>
			    <td><?php echo h($user['User']['username']); ?>&nbsp;</td>
			</tr>
			<tr>
			    <td><strong><?php echo __('Password'); ?></strong></td>
			    <td><?php echo h($user['User']['password']); ?>&nbsp;</td>
			</tr>
			<tr>
			    <td><strong><?php echo __('Role'); ?></strong></td>
			    <td><?php echo h($user['User']['role']); ?>&nbsp;</td>
			</tr>
			<tr>
			    <td><strong><?php echo __('Created'); ?></strong></td>
			    <td><?php echo h($user['User']['created']); ?>&nbsp;</td>
			</tr>
			<tr>
			    <td><strong><?php echo __('Modified'); ?></strong></td>
			    <td><?php echo h($user['User']['modified']); ?>&nbsp;</td>
			</tr>
                    </tbody>
                </table>
            </div>
			
        </div>

			
    </div>
</div>
