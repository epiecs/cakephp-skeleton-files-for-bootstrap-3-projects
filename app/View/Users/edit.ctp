<div id="page-container" class="row">
    <div id="sidebar" class="col-sm-3">
        <div class="actions">
            <ul class="list-group">
		<li class="list-group-item"><?php echo $this->Form->postLink(__('Deze %s verwijderen', $singname), array('action' => 'delete', $this->Form->value('User.id')), null, __('Ben je zeker dat je %s wil verwijderen?', $this->Form->value('User.id'))); ?></li>
		<li class="list-group-item"><?php echo $this->Html->link(__('Toon alle %s', $plurname), array('action' => 'index')); ?></li>
            </ul>	
        </div>
    </div>
	
    <div id="page-content" class="col-sm-9">
                
        <h2><?php echo __('Een %s aanpassen', $singname); ?></h2>

       	
        <?php echo $this->BootstrapForm->create('User', array('role' => 'form', 'novalidate' => true)); ?>

	    <?php echo $this->BootstrapForm->input('id', array('class' => 'form-control')); ?>
	    <?php echo $this->BootstrapForm->input('username', array('class' => 'form-control', 'label' => 'Gebruikersnaam')); ?>
	    <?php echo $this->BootstrapForm->input('password', array('class' => 'form-control', 'label' => 'Wachtwoord')); ?>
	    <?php echo $this->BootstrapForm->input('role', array(
                    'class' => 'form-control',
                    'options' => array('admin' => 'Admin', 'user' => 'Gebruiker'),
                    'label' => 'Rol'
                )); 
            ?>

	<?php echo $this->BootstrapForm->submit('Opslaan', array('class' => 'btn btn-large btn-primary')); ?>

	<?php echo $this->BootstrapForm->end(); ?>		
    </div>
</div>