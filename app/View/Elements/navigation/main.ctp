<div class="navbar-header">
    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target=".navbar-collapse">
        <span class="sr-only">Navigatie openen</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
    </button>
    <a class="navbar-brand" href="/admin">
        Admin
    </a>
</div>
<div class="navbar-collapse collapse">
    <ul class="nav navbar-nav">
        <li class="dropdown <?php if($this->params['controller'] == 'users'){echo 'active';} ?>">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">Gebruikers <span class="caret"></span></a>
            <ul class="dropdown-menu" role="menu">
                <li>
                    <?php echo $this->Html->link(__('Beheren'), array('controller' => 'users', 'action' => 'index')); ?>
                </li>
                <li>
                    <?php echo $this->Html->link(__('Toevoegen'), array('controller' => 'users', 'action' => 'add')); ?>
                </li>
            </ul>
        </li>
    </ul>
    <ul class="nav navbar-nav navbar-right">
        <li class="dropdown">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown"><?php echo ucfirst(AuthComponent::user('username')); ?> <span class="caret"></span></a>
            <ul class="dropdown-menu" role="menu">
                <li>
                    <?php echo $this->Html->link(__('Mijn account'), array('controller' => 'users', 'action' => 'edit', AuthComponent::user('id'))); ?>
                </li>
                <li class="divider"></li>
                <li>
                    <?php echo $this->Html->link(__('Uitloggen'), array('controller' => 'users', 'action' => 'logout')); ?>
                </li>
            </ul>
        </li>
        
    </ul>
</div><!--/.nav-collapse -->