<?php
    class DATABASE_CONFIG {

        /**
         *  Hier nog jouw live omgeving invullen 
         */
        
        public $default = array(
            'datasource' => 'Database/Mysql',
            'persistent' => false,
            'host' => 'localhost',
            'login' => '',
            'password' => '',
            'database' => '',
        );
        
        public $CURRENTUSERNAME = array(
            'datasource' => 'Database/Mysql',
            'persistent' => false,
            'host' => 'localhost',
            'login' => 'LOGIN',
            'password' => 'PASSWORD',
            'database' => 'DATABASENAME',
        );
    
        public function __construct() {

            Configure::write('debug', 2);

            //read application_env
            $userconfig = getenv('APPLICATION_ENV') ? getenv('APPLICATION_ENV') : 'default';
            //read dos shell
            $userconfig = isset($_SERVER['USERNAME']) ? $_SERVER['USERNAME'] : $userconfig;
            //read cygwin or lin/osx shell
            $userconfig = isset($_SERVER['USER']) ? $_SERVER['USER'] : $userconfig;

            switch ($userconfig)
            {
                case 'CURRENTUSERNAME' :
                    $this->default = $this->CURRENTUSERNAME;
                    break;
                default:
                    Configure::write('debug', 0);
                    $this->default = $this->default;
                break;
            }
        }
            
    }
    
    
?>